<?php

define ('CURRENT_URL', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

echo '<head>
    <meta charset="utf-8" />
    <link href="'.'http://'.$_SERVER['HTTP_HOST'].'/'.explode("/",$_SERVER['REQUEST_URI'])[1].'/css/style.css" rel="stylesheet" />
    </head>
    <body>';

function showPath ($url,$path = '.') {
    if (file_exists($path . "/marker.php")) {
        $obj = readInfo($path);
        $url = substr($url,0,strrpos ( $url , "/"));
        return "<a href='{$url}'>{$obj['name_rus']}</a>";
    }

    if ($path==".") {
        if (file_exists($path . "./marker.php")) {
            $obj = readInfo($path);
            $url = substr($url,0,strrpos ( $url , "/"));
            $temp = " / <a href='{$url}'>{$obj['name_rus']}</a>";

            $path="../";
            $url = substr($url,0,strrpos ( $url , "/"));
            $obj = readInfo($path);
            return "<a href='{$url}'>{$obj['name_rus']}</a>" . $temp ;
        }
        else {
            $obj = readInfo();
            $url = substr($url,0,strrpos ( $url , "/"));
            $temp = " / <a href='{$url}'>{$obj['name_rus']}</a>";

            $path="../";
            $obj = readInfo($path);
            $url = substr($url,0,strrpos ( $url , "/"));
            return showPath($url,"../../") . " / <a href='{$url}'>". $obj['name_rus']."</a>{$temp}";
        }
    }

    $obj = readInfo($path);
    $url = substr($url,0,strrpos ( $url , "/"));
    return showPath($url, $path . "../") . " / <a href='{$url}'>{$obj['name_rus']}</a>";
};

function readInfo ($path = ".") {
    $json_string = file_get_contents($path . '/info.php',null,null,14);
    return json_decode($json_string, true);
}

function showContent ($info, $path = CURRENT_URL, $selected = "") {
    echo "<br>";
    if ($info['isPhoto']==0) {
        if ($info["items"] != 0) {

            if (count($info["catalogs"]) != 0) {
                foreach ($info["catalogs"] as $cat) {
                    echo "<a href='{$path}" . $cat['name_eng']."'><div class='catalog'><img src='http://".$_SERVER['HTTP_HOST'].'/'.explode("/",$_SERVER['REQUEST_URI'])[1]."/css/directory.png'><p>{$cat['name_rus']}</p></div></a>";
                }
            }

            if (count($info["photos"]) != 0) {
                foreach ($info["photos"] as $fot) {
                    echo "<a href='{$path}" . $fot["name_eng"] . "'><div class='catalog";
                    if ($selected == ($path . $fot["name_eng"] . "/")) {
                        echo ' selected';
                    }
                    echo "'><img src='".$path.$fot['name_eng']."/".$fot['name_eng'].".jpg'><p>{$fot["name_rus"]}.jpg</p></div></a>";
                }
            }
        }

        else {
            echo "<br>Каталог пуст";
        }
    }

    else {
        echo "<img src='./" . $info['name_eng'] . ".jpg'>";
        showContent(readInfo("../"), substr(CURRENT_URL,0,strrpos (rtrim(CURRENT_URL,"/") , "/")).'/', CURRENT_URL);
    }

}



echo showPath(CURRENT_URL);
echo showContent( readInfo() );
?>
</body>