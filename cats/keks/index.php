<?php

function getRoot($path = ".") {
    if (file_exists($path . "/marker.php")) {
        return $path . "/func/kernel.php";
    }
    if ($path == ".") {
        if (file_exists($path . "./marker.php")) {
            return $path . "./func/kernel.php";
        }
        else {
            $path="..";
        }
    }
    return getRoot($path."/..");
}

include (getRoot());